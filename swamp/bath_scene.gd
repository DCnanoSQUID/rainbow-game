extends Node2D

var frog_diving = false

func pervert_incoming(body):
	$Frog/Animations.play("hide")
	frog_diving = true

func pervert_gone(body):
	if frog_diving:
		$Frog/Animations.play("unhide")
		yield($Frog/Animations, "animation_finished")
		$Frog/Animations.play("wiggle")
		frog_diving = false

func surprise(body):
	$Frog/Animations.play("surprised")
	frog_diving = true
