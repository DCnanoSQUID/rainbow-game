extends Node2D

export(String, MULTILINE) var description setget set_description, get_description

func accept():
	Questlog.add_quest(self)

func is_accepted():
	Questlog.has_quest(self)

func get_description():
	return description

func set_description(new_description):
	description = new_description
	Questlog.update_quest(self)
