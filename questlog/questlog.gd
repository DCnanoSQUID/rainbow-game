extends CanvasLayer

func _process(delta):
	if Input.is_action_just_pressed("questlog"):
		toggle_visibility()

func toggle_visibility():
	if $PopupDialog.visible:
		game.resume(self)
		$PopupDialog.hide()
	else:
		game.pause(self)
		$PopupDialog.show()

func add_quest(new_quest):
	if has_quest(new_quest):
		return
		
	var quest_item = RichTextLabel.new()
	quest_item.bbcode_enabled = true
	quest_item.bbcode_text = new_quest.description
	quest_item.set_meta("quest_id", new_quest.get_instance_id())
	quest_item.rect_min_size.x = 400
	quest_item.rect_min_size.y = 100
	$PopupDialog/VBoxContainer.add_child(quest_item)

func has_quest(quest):
	var quest_items = $PopupDialog/VBoxContainer.get_children()
	var quest_id = quest.get_instance_id()
	
	for item in quest_items:
		if item.get_meta("quest_id") == quest_id:
			return true
	
	return false

func update_quest(quest):
	var quest_items = $PopupDialog/VBoxContainer.get_children()
	var quest_id = quest.get_instance_id()
	
	var existing_item
	for item in quest_items:
		if item.get_meta("quest_id") == quest_id:
			existing_item = item
			break
	
	if existing_item == null:
		return
	
	existing_item.bbcode_text = quest.description

func collect_ingredient(aura_type):
	for node in $PopupDialog/Ingredients.get_children():
		if node.aura_type == aura_type:
			node.collect()
			toggle_visibility()
			break
