extends CanvasLayer

export(String, MULTILINE) var dialog_text = '' setget set_dialog_text, get_dialog_text
export var first_button_text = '' setget set_first_button_text, get_first_button_text
export var second_button_text = '' setget set_second_button_text, get_second_button_text

signal first_button_pressed
signal second_button_pressed

func get_dialog_text():
	return $PopupDialog/RichTextLabel.bbcode_text

func set_dialog_text(new_text):
	$PopupDialog/RichTextLabel.bbcode_text = new_text

func get_first_button_text():
	return $PopupDialog/FirstButton.text

func set_first_button_text(new_text):
	$PopupDialog/FirstButton.text = new_text
	$PopupDialog/FirstButton.visible = new_text != ''

func get_second_button_text():
	return $PopupDialog/SecondButton.text

func set_second_button_text(new_text):
	$PopupDialog/SecondButton.text = new_text
	$PopupDialog/SecondButton.visible = new_text != ''

func _on_FirstButton_pressed():
	emit_signal("first_button_pressed")
	$PopupDialog.hide()

func _on_SecondButton_pressed():
	emit_signal("second_button_pressed")
	$PopupDialog.hide()

func _on_PopupDialog_about_to_show():
	game.pause(self)

func _on_PopupDialog_popup_hide():
	game.resume(self)

func show():
	$PopupDialog.popup()
