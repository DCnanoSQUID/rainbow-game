extends Node2D

func follow():
	$StaticBody2D.queue_free()
	$CommunicationArea.queue_free()
	Global.find_player().add_follower($Follower)
