extends Node

enum AuraType {
	DESERT = 0,
	RUINS = 10,
	SWAMP = 20,
	VOLCANO = 30,
	WATER = 40,
	WOODS = 50
}

func find_aura_type_name(aura_type):
	for name in AuraType.keys():
		if AuraType.get(name) == aura_type:
			return name
	return null

func find_player() -> Player:
	return get_node("/root/Main/Player") as Player
