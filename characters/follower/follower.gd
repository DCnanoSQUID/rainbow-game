extends Node2D

export var desired_distance = Vector2(100, 100)
export var move_in_line_speed = Vector2(5, 5)

var leader : Node2D setget start_following, get_leader

func get_leader():
	return leader
	
func has_leader():
	return leader != null

func start_following(new_leader):
	if new_leader == null:
		stop_following()
		return

	leader = new_leader

func stop_following():
	leader = null

func _physics_process(delta):
	move_to_desired_position()

func move_to_desired_position():
	if !has_leader():
		return

	var current_distance = leader.global_position - get_parent().global_position
	var target_direction = Vector2(
		clamp(current_distance.x, -desired_distance.x, desired_distance.x),
		clamp(current_distance.y, -desired_distance.y, desired_distance.y)
	)
	var movement = current_distance - target_direction
	if movement.length_squared() < 2:
		return
	
	# reduce diagonal movement
	if abs(movement.x) < 1:
		movement.x = clamp(current_distance.x, -move_in_line_speed.x, move_in_line_speed.x)
	
	if abs(movement.y) < 1:
		movement.y = clamp(current_distance.y, -move_in_line_speed.y, move_in_line_speed.y)
			
	get_parent().global_position += movement
