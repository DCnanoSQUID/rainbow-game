extends Light2D

const AuraType = Global.AuraType
export(AuraType) var aura_type setget set_aura_type, get_aura_type

var minimum_scale = 0.5
var maximum_scale = 1.5

var scale_reduction_rate_per_second = 0.1
var scale_increment_per_keypress = 1.5

func _process(delta):
	var scale_reduction = scale_reduction_rate_per_second * delta
	var target_scale = max(minimum_scale, scale.x - scale_reduction)
	scale = Vector2(target_scale, target_scale)

func _input(event):
	if event.is_action_pressed("love"):
		var target_scale = min(scale_increment_per_keypress * scale.x, maximum_scale)
		$Tween.interpolate_property(self, "scale", scale, Vector2(target_scale, target_scale), 0.3)
		$Tween.start()

func light_mask_for_aura_type(aura_type):
	match aura_type:
		AuraType.RUINS:
			return pow(2, 5) as int
		AuraType.SWAMP:
			return pow(2, 6) as int
		AuraType.VOLCANO:
			return pow(2, 7) as int
		AuraType.WATER:
			return pow(2, 8) as int
		AuraType.WOODS:
			return pow(2, 9) as int

func set_aura_type(new_type):
	var aura_type_name = Global.find_aura_type_name(new_type)
	if aura_type_name == null:
		return call_deferred("print_aura_type_error")

	aura_type = new_type

	var music_file_name = aura_type_name.to_lower() + ".ogg"
	$Music.stream = load("res://music/" + music_file_name)
	$Music.play(DesertMusic.get_playback_position())
	call_deferred("fade_in_music")
	
	var light_mask = light_mask_for_aura_type(aura_type)
	if light_mask == null:
		return call_deferred("print_aura_type_error")
	range_item_cull_mask |= light_mask

func print_aura_type_error():
	print("Unexpected aura type for " + get_path())

func get_aura_type():
	return aura_type

func fade_in_music():
	var start_volume = -40
	var end_volume = 0
	var duration_in_seconds = 2
	
	var tween = Tween.new()
	add_child(tween)

	tween.interpolate_property(
		$Music,
		"volume_db",
		start_volume,
		end_volume,
		duration_in_seconds
	)
	tween.start()
