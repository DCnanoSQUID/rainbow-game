tool
extends Node2D

export var area_scale = Vector2(1,1)

signal talk

func _process(delta):
	if scale != Vector2(1, 1):
		push_error("Please don't scale me, I have an area scale property")
		assert(false)
	
	$TriggerArea.scale = area_scale

func speech_bubble_pressed():
	emit_signal("talk")
