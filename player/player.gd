extends KinematicBody2D
class_name Player

# Player movement speed
export var speed: int

var last_follower : Node2D

func _input(event):
	if event.is_action_pressed("love"):
		var explosion = preload("res://player/heart_explosion.tscn").instance()
		explosion.position = position + Vector2(0, -50)
		get_parent().add_child(explosion)

func _physics_process(delta):
	# Get player input
	var direction: Vector2
	direction.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	direction.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	if abs(direction.x) == 1 and abs(direction.y) == 1:
		direction = direction.normalized()
	
	var speed_multiplier = 1
	if Input.is_action_pressed("sprint"):
		speed_multiplier = 3

	var movement = speed * speed_multiplier * direction * delta
	move_and_slide(movement)

	# Animate player based on direction
	animates_player(direction)
	
	$FootStepRotator.rotation = direction.angle() + PI/2.0

func animates_player(direction: Vector2):
	if direction.length() == 0:
		$AnimationTree.get("parameters/playback").travel("idle")
		if $Steps.playing:
			$Steps.stop()
		$FootStepRotator/FootSteps.emitting = false
	else:
		$AnimationTree.get("parameters/playback").travel("walk")
		$AnimationTree.set("parameters/walk/blend_position", direction)
		$AnimationTree.set("parameters/idle/blend_position", direction)
		if not $Steps.playing:
			$Steps.play()
		$FootStepRotator/FootSteps.emitting = true

func add_follower(new_follower):
	var leader
	if last_follower == null:
		leader = self
	else:
		leader = last_follower.get_parent()

	new_follower.start_following(leader)
	last_follower = new_follower
