extends Area2D

signal player_entered
signal player_exited

func body_entered(body):
	emit_signal("player_entered")

func body_exited(body):
	emit_signal("player_exited")
