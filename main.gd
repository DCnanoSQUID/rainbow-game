extends Node2D

func _process(delta):
	if Input.is_action_just_pressed("mute"):
		var master_channel = 0
		var should_mute = !AudioServer.is_bus_mute(master_channel)
		AudioServer.set_bus_mute(master_channel, should_mute)
