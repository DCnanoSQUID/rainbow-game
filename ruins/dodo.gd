extends StaticBody2D

export var translation_speed = 1;
var direction = 1
#export var translation_speed = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	
	if direction == 1:
		position.x = position.x + translation_speed
		#$AnimatedSprite.play("left")
		
		if position.x > 1500:
			direction = -1
			$AnimatedSprite.flip_h = false
		
	
	elif direction == -1 :
		position.x = position.x - translation_speed
	
		if position.x < -50:
			direction = 1
			$AnimatedSprite.flip_h = true
			
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
